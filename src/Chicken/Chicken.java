package Chicken;
import Game.*;

import javax.swing.*;

public abstract class Chicken implements Visitor {
	private final int NUM;
	protected JLabel img;
	protected boolean die=false;
	protected Board board;
	protected final int RED=1,BLUE=2,YELLOW=3,ORANGE=4,PURPLE=5,GREEN=6;
	int x,y;
	
	public Chicken(int num ,Board board,int row, int colum){
		this.NUM=num;
		this.board = board;
		this.x =row;
		this.y = colum;
	}
	
	public JLabel getIMG(){
		return img;
	}

	public int getNum(){
		return NUM;
	}
	
	public boolean isDie(){
		return die;
		
	}
	
	public void kill(){
		if(!die){
			this.die = true;
			board.kill(this);	
		}
		
		
	}
	
	
}
