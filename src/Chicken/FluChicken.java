package Chicken;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Board;
import Shot.*;

public class FluChicken extends Chicken {
	
	
	public FluChicken(int type,Board board,int row, int colum){
		super(type,board,row,colum);
		super.img =new JLabel(new ImageIcon(Chicken.class.getResource("/Resources/chicken/Bonus/the_chicken_flu.png")));
	}
	
	@Override
	public void gotShot(RedShot shot){
	}

	@Override
	public void gotShot(BlueShot shot) {	
	}



	@Override
	public void gotShot(YellowShot shot) {
	}

	@Override
	public void gotShot(BlackShot shot) {
		board.killAll();
	}

	
	

}
