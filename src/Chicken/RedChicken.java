package Chicken;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Board;
import Shot.*;

public class RedChicken extends Chicken {
	
	
	public RedChicken(int type,Board board,int row, int colum){
		super(type,board,row,colum);
		super.img =new JLabel(new ImageIcon(Chicken.class.getResource("/Resources/chicken/chicken_red.PNG")));
	}
	
	@Override
	public void gotShot(RedShot shot){
		super.kill();
	}

	@Override
	public void gotShot(BlueShot shot) {
		board.killOthers(PURPLE);
	}



	@Override
	public void gotShot(YellowShot shot) {
		board.killOthers(ORANGE);
	}

	@Override
	public void gotShot(BlackShot shot) {
		// TODO Auto-generated method stub
	}

	
	

}
