package Chicken;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Board;
import Shot.*;

public class XChicken extends Chicken {
	
	
	public XChicken(int type,Board board,int row, int colum){
		super(type,board,row,colum);
		super.img =new JLabel(new ImageIcon(Chicken.class.getResource("/Resources/chicken/special/chicken_x.png")));
	}
	
	@Override
	public void gotShot(RedShot shot){
	}

	@Override
	public void gotShot(BlueShot shot) {	
	}

	@Override
	public void gotShot(YellowShot shot) {
	}

	@Override
	public void gotShot(BlackShot shot) {
		super.kill();
		board.killDiagonal(x,y);
	}

	
	

}
