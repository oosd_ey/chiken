package Chicken;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Board;
import Shot.*;

public class RowChicken extends Chicken {
	
	
	public RowChicken(int type,Board board,int row, int colum){
		super(type,board,row,colum);
		super.img =new JLabel(new ImageIcon(Chicken.class.getResource("/Resources/chicken/Bonus/chicken_row.PNG")));
	}
	
	@Override
	public void gotShot(RedShot shot){
	}

	@Override
	public void gotShot(BlueShot shot) {	
	}



	@Override
	public void gotShot(YellowShot shot) {
	}

	@Override
	public void gotShot(BlackShot shot) {
		super.kill();
		board.killRow(x,y);
	}

	
	

}
