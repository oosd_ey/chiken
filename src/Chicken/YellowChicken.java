package Chicken;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Board;
import Shot.*;

public class YellowChicken extends Chicken {
	
	
	public YellowChicken(int type,Board board,int row, int colum){
		super(type,board,row,colum);
		super.img =new JLabel(new ImageIcon(Chicken.class.getResource("/Resources/chicken/chicken_yellow.PNG")));
		
	}
	
	@Override
	public void gotShot(RedShot shot){
		board.killOthers(ORANGE);
	}

	@Override
	public void gotShot(BlueShot shot) {
		board.killOthers(GREEN);
	}


	@Override
	public void gotShot(YellowShot shot) {
		super.kill();
	}



	@Override
	public void gotShot(BlackShot shot) {
		// TODO Auto-generated method stub
	}

	
	

}
