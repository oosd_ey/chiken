package Game;

import javax.swing.*;

import Chicken.Chicken;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MainMenu extends JFrame implements ActionListener {
	
	private Game currentGame;
	private JButton levelOne,levelTwo, levelThree,levelFour,levelFive,levelSix,btn_lastGames ,btn_exit;
	private int totalScore;
	private final int[][]
			LEVELONE ={{1,2,3,1,1,3,2,1},{3,1,2,2,2,2,1,3},{2,3,1,3,3,1,3,2},{1,2,3,4,4,3,2,1}},
			LEVELTWO = {{5,5,5,6,6,4,4,4},{3,9,1,1,1,1,9,3},{3,1,2,2,2,2,1,3},{6,5,4,3,3,4,5,6}},
			LEVELTHREE = {{5,5,5,3,3,4,4,4},{5,8,5,2,2,6,6,6},{5,5,5,1,1,6,8,6},{4,4,4,8,4,6,6,6}},
			LEVELFOUR = {{5,5,4,4,5,5,4,4},{6,7,6,7,6,7,6,7},{4,3,5,2,4,3,5,2},{1,1,1,1,1,1,1,1}},
			BONUS1 = {{5,5,4,10,5,5,4,4},{6,12,6,7,11,7,6,7},{4,3,5,2,4,3,13,2},{1,1,1,12,1,1,1,1}},
			BONUS2 = {{2,12,3,4,1,3,2,1},{9,2,10,4,11,5,12,6},{6,6,4,7,2,7,3,8},{1,1,2,2,3,4,5,4}}; 

	public MainMenu(){
		   super("Chicken Inverse");
		   getContentPane().setBackground(new Color(0, 0, 51));
		   this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	  
		   this.setMinimumSize(new Dimension(1000,800));
		   setResizable(false);
		   
		   levelOne = new JButton("Level One");
		   levelOne.setBackground(new Color(204, 0, 0));
		   levelOne.setForeground(new Color(255, 255, 255));
		   levelOne.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   levelOne.setBounds(200, 220, 200, 50);
		   levelOne.addActionListener(this);
		   getContentPane().setLayout(null);
		   getContentPane().add(levelOne);
		   
		   levelTwo = new JButton("Level Two");
		   levelTwo.setForeground(new Color(255, 255, 255));
		   levelTwo.setBackground(new Color(255, 204, 0));
		   levelTwo.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   levelTwo.setBounds(600, 220, 200, 50);
		   levelTwo.addActionListener(this);
		   getContentPane().add(levelTwo);
		   
		   levelThree = new JButton("Level Three");
		   levelThree.setForeground(new Color(255, 255, 255));
		   levelThree.setBackground(new Color(204, 0, 0));
		   levelThree.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   levelThree.setBounds(200, 350, 200, 50);
		   levelThree.addActionListener(this);
		   getContentPane().add(levelThree);
		   
		   levelFour = new JButton("Level Four");
		   levelFour.setBackground(new Color(255, 204, 0));
		   levelFour.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   levelFour.setForeground(new Color(255, 255, 255));
		   levelFour.setBounds(600, 350, 200, 50);
		   levelFour.addActionListener(this);
		   getContentPane().add(levelFour);
		   
		   levelFive = new JButton("Bonus One");
		   levelFive.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   levelFive.setForeground(new Color(255, 255, 255));
		   levelFive.setBackground(new Color(204, 0, 0));
		   levelFive.setBounds(200, 500, 200, 50);
		   levelFive.addActionListener(this);
		   getContentPane().add(levelFive);
		   
		   levelSix = new JButton("Bonus Two");
		   levelSix.setForeground(new Color(255, 255, 255));
		   levelSix.setBackground(new Color(255, 204, 0));
		   levelSix.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   levelSix.setBounds(600, 500, 200, 50);
		   levelSix.addActionListener(this);
		   getContentPane().add(levelSix);
		   
		   btn_lastGames= new JButton("Last Games");
		   btn_lastGames.setForeground(new Color(255, 255, 255));
		   btn_lastGames.setBackground(new Color(204, 0, 0));
		   btn_lastGames.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   btn_lastGames.setBounds(200, 650, 200, 50);
		   btn_lastGames.addActionListener(this);
		   getContentPane().add(btn_lastGames);
		   
		   btn_exit = new JButton("Exit");
		   btn_exit.setForeground(new Color(255, 255, 255));
		   btn_exit.setBackground(new Color(255, 204, 0));
		   btn_exit.setFont(new Font("Tahoma", Font.PLAIN, 25));
		   btn_exit.setBounds(600, 650, 200, 50);
		   btn_exit.addActionListener(this);
		   getContentPane().add(btn_exit);
		   
		   JLabel logo = new JLabel(new ImageIcon(Chicken.class.getResource("/Resources/logo.png")));
		   logo.setBounds(309, 0, 399, 200);
		   getContentPane().add(logo);
		   
		   this.setVisible(true);
	}
	
	
	
	public void actionPerformed(ActionEvent e){
		 if (e.getSource() == levelOne){
			 totalScore=0;
			 currentGame = new Game(1,LEVELONE,this);
			 this.setVisible(false);
			 currentGame.setLocation(this.getLocation());
			 currentGame.setVisible(true);
		 }
		 
		 if (e.getSource() == levelTwo){
			  totalScore=0;
			  currentGame = new Game(2,LEVELTWO,this);
			  this.setVisible(false);
			  currentGame.setLocation(this.getLocation());
			  currentGame.setVisible(true);
		 }
		 
		 if (e.getSource() == levelThree){
			  totalScore=0;
			  currentGame = new Game(3,LEVELTHREE,this);
			  this.setVisible(false);
			  currentGame.setLocation(this.getLocation());
			  currentGame.setVisible(true);
		 }
		 
		 if (e.getSource() == levelFour){
			  totalScore=0;
			  currentGame = new Game(4,LEVELFOUR,this);
			  this.setVisible(false);
			  currentGame.setLocation(this.getLocation());
			  currentGame.setVisible(true);
		 }
		 
		 if (e.getSource() == levelFive){
			 totalScore=0; 
			 currentGame = new Game(5,BONUS1,this);
			  this.setVisible(false);
			  currentGame.setLocation(this.getLocation());
			  currentGame.setVisible(true);
		 }
		 
		 if (e.getSource() == levelSix){
			 totalScore=0; 
			 currentGame = new Game(6,BONUS2,this);
			  this.setVisible(false);
			  currentGame.setLocation(this.getLocation());
			  currentGame.setVisible(true);
		 }
		 
		 if (e.getSource() == btn_lastGames){
			 lastGames();
		 }
		 
		 if (e.getSource() == btn_exit){
			 dispose();
		 }
	}




	public static void main(String[] args) {
		new MainMenu();

	}
	
	public int getScore(){
		return totalScore; 
	}
	
	public void setScore(int score){
		this.totalScore += score;
	}
	
	public void nextLevel(int level){
		switch (level){
		case 1:
			  currentGame = new Game(2,LEVELTWO,this);
			  break;
		case 2:
			  currentGame = new Game(3,LEVELTHREE,this);
			  break;
		case 3:
			  currentGame = new Game(4,LEVELFOUR,this);
			  break;
		case 4:
			  currentGame = null;
			  saveToLast(totalScore);
			  break;
			  
		default:
		      currentGame = null;
		      break;
		}
		
		if (currentGame != null){
			this.setVisible(false);
			  currentGame.setSize(this.getSize());
			  currentGame.setLocation(this.getLocation());
			  currentGame.setVisible(true);
		}
		else
			this.setVisible(true);
		
	}
	
	// save the finish game to the last games
	private void saveToLast(int score){ 
	   // save to file
	   try
	   {
		   String name ="Player";
		   DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		   name= JOptionPane.showInputDialog("Please enter your Name: ");
		   Calendar cal = Calendar.getInstance();
		   String toAdd = name+"-"+score+"-"+dateFormat.format(cal.getTime())+"-";

		   File file = new File("save.txt");
		   String txt = new String(Files.readAllBytes(Paths.get(file.getPath()))); // the text from the file
		   String[] parts = txt.split("-");
		   if(parts.length < 15) // not all the 5 last is full
			   txt=toAdd+txt;
		   else{  
			   txt=toAdd;
			   for(int i=0 ; i<parts.length-3 ; i++)   // give me the last 4 
				   txt+=parts[i]+"-";
		   }
		   FileWriter fw = new FileWriter(file);
		   fw.write(txt);
		   fw.flush();
		   fw.close();
		   lastGames();
	   }
	   catch (Exception er) {
		   JOptionPane.showMessageDialog(this, "Problem on Saving to Last Games"); 
	   }  
		
	}
	
	private void lastGames(){
			// load from file
		   try
		   {
			   File file = new File("save.txt");
			   String txt = new String(Files.readAllBytes(Paths.get(file.getPath())));
			   String[] parts = txt.split("-");
			   String lastGamesTxt="";
			   for(int i=0; parts.length>2 && i<parts.length; i=i+3){
				   lastGamesTxt+="Player name : "+parts[i]+" Score : "+parts[i+1]+" Time : "+parts[i+2]+"\n";
			   }
			   JOptionPane.showMessageDialog(this, "Last Games : \n"+lastGamesTxt); 
		   }
		   catch (Exception er) {
			   JOptionPane.showMessageDialog(this, "Problem on Loading Last Games"); 
		   }  
		
	}
}


