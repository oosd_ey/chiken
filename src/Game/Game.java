package Game;

import java.awt.Dimension;

import javax.swing.*;


import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Game extends JFrame {
	private int level;
	private Board board;
	private int shotCount=0 , time=0;
	private JLabel lblSh , shotIcon ,labelTime;
	private Timer timer;
	private MainMenu menu;

	public Game(int level , int[][] chickens, MainMenu menu){
		super("Chicken Invaders");
		this.menu = menu;
		this.level = level;
		getContentPane().setBackground(new Color(0, 0, 51));
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	  
		this.setMinimumSize(new Dimension(1200,800));
		setResizable(false);
		getContentPane().setLayout(null);
		
		JPanel status = new JPanel();
		status.setBackground(new Color(0, 0, 51));
		status.setBounds(0, 0, 297, 80);
		getContentPane().add(status);
		status.setLayout(null);
		
		JButton btnReturnManu = new JButton("Return Manu");
		btnReturnManu.setBounds(0, 0, 153, 80);
		btnReturnManu.setFocusable(false);
		btnReturnManu.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				menu.setLocation(getLocation());
				menu.setVisible(true);	
			}
		});
		status.add(btnReturnManu);
		
		lblSh = new JLabel();
		lblSh.setText("Shots: "+ shotCount);
		lblSh.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblSh.setForeground(new Color(255, 255, 255));
		lblSh.setBounds(162, 24, 160, 26);
		status.add(lblSh);
		
		

		JPanel board_game = new JPanel();
		board_game.setBounds(10, 80, 1150, 650);
		getContentPane().add(board_game);
		board_game.setLayout(new GridLayout(0, 1, 0, 0));
		board= new Board(chickens,this);
		board_game.add(board);
		
		shotIcon = new JLabel();
		shotIcon.setBounds(297, 0, 92, 80);
		getContentPane().add(shotIcon);
		
		JLabel lblTimer = new JLabel("Time :");
		lblTimer.setForeground(Color.WHITE);
		lblTimer.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTimer.setBounds(426, 0, 92, 80);
		getContentPane().add(lblTimer);
		
		labelTime = new JLabel(time+"");
		labelTime.setForeground(Color.YELLOW);
		labelTime.setFont(new Font("Tahoma", Font.PLAIN, 30));
		labelTime.setBounds(548, 0, 92, 80);
		getContentPane().add(labelTime);
		
		JLabel lblSeconds = new JLabel("Seconds");
		lblSeconds.setForeground(Color.WHITE);
		lblSeconds.setBounds(661, 33, 92, 26);
		getContentPane().add(lblSeconds);
		
		JLabel lblLevel = new JLabel("Level :");
		lblLevel.setForeground(Color.WHITE);
		lblLevel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblLevel.setBounds(785, 0, 92, 80);
		getContentPane().add(lblLevel);
		
		JLabel lblNewLevel = new JLabel(level+"");
		lblNewLevel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLevel.setForeground(Color.RED);
		lblNewLevel.setBounds(906, 0, 92, 80);
		getContentPane().add(lblNewLevel);
		
		
	}
	
	public void shotCounter(){
		this.shotCount++;
		lblSh.setText("Shots: "+ shotCount);	
	}
	
	public void gameOver(){
		int levelScore = Math.max(0,600-(shotCount*10)-time);
		menu.setScore(levelScore);
		JOptionPane.showMessageDialog(this,
				"Level Time: " +time+ "\n"
				+ "Level Shots: " +shotCount+ "\n"
				+"Level Score: "+ levelScore+ "\n"
				+"Total Score: "+ menu.getScore(),
				"Level Complete !!!",
			    JOptionPane.PLAIN_MESSAGE);
		menu.setLocation(this.getLocation());
		this.setVisible(false);
		menu.nextLevel(level);
	}
	
	/**
	 * update the active shot image
	 * @param icon the active shot icon
	 */
	public void updateShot(Icon icon){
		shotIcon.setIcon(icon);
		repaint();
	}
	
	//start game
	public void startGame(){
		timer = new Timer(1000, new ActionListener() {
		    public void actionPerformed(ActionEvent ae) {
		    	
		    	if(board.isGameFinish()){
		    		timer.stop();
		    		/// make
		    	}
		    	else{
		    		time++;
		    		labelTime.setText(time+"");
		    	}
		    		
		    }
		});
		timer.start();
		time++; //for first time
		labelTime.setText(time+"");
	}
}
