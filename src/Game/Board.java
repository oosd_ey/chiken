package Game;

import javax.swing.*;

import Shot.BlackShot;
import Shot.BlueShot;
import Shot.RedShot;
import Shot.Shot;
import Shot.YellowShot;
import Chicken.*;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Board extends JPanel implements KeyListener{
	private Chicken[][] chickenArr;
	private JPanel chi_Panel;
	private JLabel ship , shot;
	private boolean activeShot=false , gameStart=false;
	private final int RED=1,BLUE=2,YELLOW=3,ORANGE=4,PURPLE=5,GREEN=6,X=7,PLUS=8,CIRCLE=9,COL=10,ROW=11,XOR=12,ALL=13;
	private Timer timer, timerExp;
	private int shotNum=1 , ChickensNum=32;
	private Shot selectedShot;
	private Game game;
	
	
	
	public Board(int[][] chickens,Game game){
		this.game=game;
		this.setSize(1150, 650);
		setBackground(new Color(0, 0, 51));
		setLayout(null);
		chi_Panel = new JPanel();
		chi_Panel.setBounds(0, 0, 1150, 650);
		chi_Panel.setBackground(new Color(0, 0, 51));
		add(chi_Panel);
		chi_Panel.setLayout(null);
		ship =new JLabel(new ImageIcon(Chicken.class.getResource("/Resources/spaceship.PNG")));
		ship.setBounds(400, 550, 116, 125);
		chi_Panel.add(ship);
		buildChickenArr(chickens);
		chi_Panel.addKeyListener(this);
		chi_Panel.setFocusable(true);
		chi_Panel.requestFocus();
		
		shot = new JLabel();
		chi_Panel.add(shot);		
	}

	private void  buildChickenArr( int[][] arr){
		chickenArr = new Chicken[4][8];
		for(int i=0; i<arr.length ; i++)
			for(int j=0 ; j<arr[i].length ; j++){
				switch (arr[i][j]){
				case RED:
					chickenArr[i][j]=new RedChicken(RED,this,j,i);
					break;
				case BLUE:
					chickenArr[i][j]=new BlueChicken(BLUE,this,j,i);
					break;
				case YELLOW:
					chickenArr[i][j]=new YellowChicken(YELLOW,this,j,i);
					break;
				case ORANGE:
					chickenArr[i][j]=new OrangeChicken(ORANGE,this,j,i);
					break;
				case PURPLE:
					chickenArr[i][j]=new PurpleChicken(PURPLE,this,j,i);
					break;
				case GREEN:
					chickenArr[i][j]=new GreenChicken(GREEN,this,j,i);
					break;
				case X:
					chickenArr[i][j]=new XChicken(X,this,j,i);
					break;
				case PLUS:
					chickenArr[i][j]=new PlusChicken(PLUS,this,j,i);
					break;
				case CIRCLE:
					chickenArr[i][j]=new CircleChicken(CIRCLE,this,j,i);
					break;
				case COL:
					chickenArr[i][j]=new ColumnChicken(COL,this,j,i);
					break;
				case ROW:
					chickenArr[i][j]=new RowChicken(ROW,this,j,i);
					break;
				case XOR:
					chickenArr[i][j]=new XorChicken(XOR,this,j,i);
					break;
				case ALL:
					chickenArr[i][j]=new FluChicken(ALL,this,j,i);
					break;

				}
				JLabel img = chickenArr[i][j].getIMG();
				chi_Panel.add(img);
				img.setBounds((140*(j%8)), 100*i,140,100);
			}	
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (Character.isSpaceChar(e.getKeyChar())) { // if user want to fire
			if(!gameStart){ // Start the Game!
				gameStart=true;
				game.startGame();
			}
			
            fire();
        }
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_1 || e.getKeyCode() == KeyEvent.VK_NUMPAD1) { //1 black
			shotNum=1;
			game.updateShot(new BlackShot().getIMG().getIcon());
		}
			
		else if (e.getKeyCode() == KeyEvent.VK_2 || e.getKeyCode() == KeyEvent.VK_NUMPAD2){ //2 red
			shotNum=2;
			game.updateShot(new RedShot().getIMG().getIcon());
		}
			
		else if (e.getKeyCode() == KeyEvent.VK_3 || e.getKeyCode() == KeyEvent.VK_NUMPAD3){ //3 blue
			shotNum=3;
			game.updateShot(new BlueShot().getIMG().getIcon());
		}
			
		else if (e.getKeyCode() == KeyEvent.VK_4 || e.getKeyCode() == KeyEvent.VK_NUMPAD4){ // 4 yellow
			shotNum=4;
			game.updateShot(new YellowShot().getIMG().getIcon());
		}
			
		else if (e.getKeyCode() == KeyEvent.VK_RIGHT){ //take ship right
			if(ship.getLocation().x < 1030){
				ship.setLocation(ship.getLocation().x+20 , ship.getLocation().y);
				repaint();
			}
		}
		
		else if (e.getKeyCode() == KeyEvent.VK_LEFT){ //take ship left
			if(ship.getLocation().x > 0){
				ship.setLocation(ship.getLocation().x-20 , ship.getLocation().y);
				repaint();
			}
		}	
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}
	
	private void fire(){
		if(!activeShot){
			activeShot=true;
			game.shotCounter();// increase shot count in game
			Point shipLocation = ship.getLocation();
			createShot();
			shipLocation.y-=10;
			shipLocation.x+=50;
			shot.setBounds(shipLocation.x, shipLocation.y, 40, 16);
			shot.setVisible(true);
			
			timer = new Timer(20, new ActionListener() {
			    public void actionPerformed(ActionEvent ae) {
			    	shipLocation.y-=5;
					shot.setLocation(shipLocation);
			    	if(tryKill()){
			    		timer.stop();
			    		/// make
			    	}
			    		
			    }
			});
			timer.start();
		}	
	}
	
	private boolean tryKill(){
		Point shotLocation= shot.getLocation();
		if(shotLocation.y <= 0) // if out screen
		{
			activeShot=false;
			shot.setVisible(false);
			return true;
		}
		
		int line=0;
		if(shotLocation.y <= 400 && shotLocation.y > 300) // line 4
			line=4;
		else if(shotLocation.y <= 300 && shotLocation.y > 200) // line 3
			line=3;
		else if(shotLocation.y <= 200 && shotLocation.y > 100) // line 2
			line=2;
		else if(shotLocation.y <= 100 && shotLocation.y > 0) // line 1
			line=1;
		
		int row=0;
		if(shotLocation.x <= 1120 && shotLocation.x > 980) // row 8
			row=8;
		else if(shotLocation.x <= 980 && shotLocation.x > 840) // row 7
			row=7;
		else if(shotLocation.x <= 840 && shotLocation.x > 700) // row 6
			row=6;
		else if(shotLocation.x <= 700 && shotLocation.x > 560) // row 5
			row=5;
		else if(shotLocation.x <= 560 && shotLocation.x > 420) // row 4
			row=4;
		else if(shotLocation.x <= 420 && shotLocation.x > 280) // row 3
			row=3;
		else if(shotLocation.x <= 280 && shotLocation.x > 140) // row 2
			row=2;
		else if(shotLocation.x <= 140 && shotLocation.x > 0) // row 1
			row=1;
		
		if (line!= 0 && row != 0 && !chickenArr[line-1][row-1].isDie() ){ // make the kill !
			selectedShot.shooting(chickenArr[line-1][row-1]);
			activeShot=false;
			shot.setVisible(false);
			return true;		
		}
		return false;	
	}
	
	public void killOthers(int num){
		List<Chicken> chickenList  = new ArrayList<Chicken>();
		for(int i=0; i<chickenArr.length ; i++){
			for(int j=0 ; j<chickenArr[i].length ; j++){
		
				if (!chickenArr[i][j].isDie() && chickenArr[i][j].getNum() == num){
				chickenList.add(chickenArr[i][j]);
				}
			}
		}
		int count=0;
		while (chickenList.size()>0 && count<2){
			int rnd = (int)(chickenList.size()*Math.random());
			chickenList.get(rnd).kill();
			chickenList.remove(rnd);
			count++;
		}
		
	}
	
	public void kill(Chicken  chicken){
		ChickensNum--;
		Icon icon = new ImageIcon(Chicken.class.getResource("/Resources/explosion.PNG"));
		chicken.getIMG().setIcon(icon);
		AudioInputStream audioIn = null;
		Clip clip = null;
		try {
			audioIn = AudioSystem.getAudioInputStream(Chicken.class.getResource("/Resources/sound/explosion.wav"));
			clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		}
		catch(Exception e){
			System.out.println("Problem With Sound");
		}
		// Disappear the bomb 
		timerExp = new Timer(100, new ActionListener() {
		    public void actionPerformed(ActionEvent ae) {
		    	chi_Panel.remove(chicken.getIMG());
				repaint();
		    }
		});
		timerExp.setRepeats(false);
		timerExp.start();
	
		
		if (ChickensNum == 0){ //if game finish
			gameStart=false;
			game.gameOver();
		}
		
	}
	
	private void createShot(){
		if(shotNum==1){
			selectedShot = new BlackShot();
			shot.setIcon(selectedShot.getIMG().getIcon());
			repaint();	
		}
			
		else if (shotNum ==2){
			selectedShot = new RedShot();
			shot.setIcon(selectedShot.getIMG().getIcon());
			repaint();
		}
			
		else if (shotNum ==3){
			selectedShot = new BlueShot();
			shot.setIcon(selectedShot.getIMG().getIcon());
			repaint();
		}
			
		else if(shotNum ==4){
			selectedShot = new YellowShot();
			shot.setIcon(selectedShot.getIMG().getIcon());
			repaint();
		}
			
		
	}
	
	// kill all chickens on Row
	public void killRow(int x,int y){
		for(int i=0; i<chickenArr[y].length ; i++)
			chickenArr[y][i].kill();
	}
	
	public void killCircle(int x,int y){
		chickenArr[y][x].kill(); //Kill the middle chicken;
		try{ 
			chickenArr[y-1][x+1].kill();	
		}
		catch(Exception e){
		}	
		try{
			chickenArr[y][x+1].kill();
		}
		catch(Exception e){
		}	
		try{
			chickenArr[y+1][x+1].kill();
		}
		catch(Exception e){
		}	
		try{
			chickenArr[y+1][x].kill();	
		}
		catch(Exception e){
		}	
		try{
			chickenArr[y+1][x-1].kill();
		}
		catch(Exception e){
		}	
		try{
			chickenArr[y][x-1].kill();
		}
		catch(Exception e){
		}	
		try{
			chickenArr[y-1][x-1].kill();
		}
		catch(Exception e){
		}	
		try{
			chickenArr[y-1][x].kill();
		}
		catch(Exception e){
		}	
	}
	
	// kill all chickens on Row
	public void killColum(int x,int y){
		for(int i=0; i<chickenArr.length ; i++)
			chickenArr[i][x].kill();
	}
	
	public void killDiagonal(int x,int y){
		chickenArr[y][x].kill();// kill the first Chicken
		for(int add=1; add<4 ;add++){
				if(y+add < 4 && y+add >=0 && x-add <8 && x-add >= 0)
					chickenArr[y+add][x-add].kill();
				if(y-add<4 && y-add >= 0 && x+add < 8 && x+add >= 0  )
					chickenArr[y-add][x+add].kill();
				if(y-add < 4 && y-add >=0 && x-add <8 && x-add >= 0)
					chickenArr[y-add][x-add].kill();
				if(y+add<4 && y+add >= 0 && x+add < 8 && x+add >= 0  )
					chickenArr[y+add][x+add].kill();		
		}
		
	}
	
	// kill all chickens
	public void killAll(){
		for(int i=0 ; i<chickenArr.length ; i++)
			for(int j=0; j<chickenArr[i].length; j++)
				chickenArr[i][j].kill();
	}
	
	public boolean isGameFinish(){
		return !gameStart;
	}
}
