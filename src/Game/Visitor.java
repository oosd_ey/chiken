package Game;
import Shot.*;

public interface Visitor {
	
	public void gotShot(RedShot shot);
	
	public void gotShot(BlueShot shot);
		
	public void gotShot(YellowShot shot);
	
	public void gotShot(BlackShot shot);
	
}
