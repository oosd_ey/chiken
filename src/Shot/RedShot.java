package Shot;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Visitor;

public class RedShot extends Shot {
	
	public RedShot(){
		super.img =new JLabel(new ImageIcon(Shot.class.getResource("/Resources/Shots/RedShot.png")));
	}

	@Override
	public void shooting(Visitor v) {
		v.gotShot(this);

	}
	
	

}
