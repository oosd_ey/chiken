package Shot;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import Game.Visitor;

public class BlackShot extends Shot {
	
	public BlackShot(){
		super.img =new JLabel(new ImageIcon(Shot.class.getResource("/Resources/Shots/BlackShot.png")));

	}

	@Override
	public void shooting(Visitor v) {
		v.gotShot(this);

	}
	
	

}
