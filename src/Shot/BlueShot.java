package Shot;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Visitor;

public class BlueShot extends Shot {
	
	public BlueShot(){
		super.img =new JLabel(new ImageIcon(Shot.class.getResource("/Resources/Shots/BlueShot.png")));
	}

	@Override
	public void shooting(Visitor v) {
		v.gotShot(this);

	}
	
	

}
