package Shot;

import javax.swing.JLabel;

import Game.Visited;
import Game.Visitor;

public abstract class Shot implements Visited {
	
	protected JLabel img;
	
	public abstract void shooting(Visitor v);
	
	public JLabel getIMG(){
		return img;
	}


}
