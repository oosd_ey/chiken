package Shot;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Game.Visitor;

public class YellowShot extends Shot {
	
	public YellowShot(){
		super.img =new JLabel(new ImageIcon(Shot.class.getResource("/Resources/Shots/YellowShot.png")));
	}

	@Override
	public void shooting(Visitor v) {
		v.gotShot(this);
	}
	
	

}
